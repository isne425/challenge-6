#include<iostream>
#include<cmath>
#include"fraction.h"

using namespace std;

void main()
{
	fraction f1(1, 2);
	fraction f2(2, 3);

	cout << "Enter 1st fraction : ";
	cin >> f1;
	cout << "1st fraction = " << f1 << endl;

	cout << "Enter 2st fraction : ";
	cin >> f2;
	cout << "2nd fraction = " << f2 << endl << endl;

	//	comparation
	if (f1 == f2)
	{
		cout << "These are the SAME!" << endl << endl;
	}
	else if (f1 >= f2)
	{
		cout << "1st fraction is greater than 2nd fraction or equal to 2nd fraction." << endl << endl;
	}
	else if (f1 <= f2)
	{
		cout << "1st fraction is less than 2nd fraction or equal to 2nd fraction." << endl << endl;
	}
	else if (f1 > f2)
	{
		cout << "1st fraction is greater than 2nd fraction." << endl << endl;
	}
	else if (f1 < f2)
	{
		cout << "1st fraction is less than 2nd fraction." << endl << endl;
	}
	else if (f1 != f2)
	{
		cout << "These are NOT the SAME!" << endl << endl;
	}

	// computation

	fraction f3 = f1 + f2;
	cout << "The sumation is " << f3 << endl;

	fraction f4 = f1 - f2;
	cout << "The difference is " << f4 << endl;

	fraction f5 = f1 * f2;
	cout << "The product is " << f5 << endl;

	fraction f6 = f1 / f2;
	cout << "The quotient is " << f6 << endl << endl;

	fraction f7 = ++f1;
	cout << "1st fraction +1 = " << f7 << endl;

	fraction f8 = ++f2;
	cout << "2st fraction +1 = " << f8 << endl << endl;

	fraction f9 = --f1;
	cout << "1st fraction -1 = " << f9 << endl;

	fraction f10 = --f2;
	cout << "2nd fraction -1 = " << f10 << endl;

	system("pause");
}


